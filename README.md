# OpenBoost

The publicly available list with free and open source projects accepting cryptocurrencies donations. 

Available at https://openboost.github.io as a website.

## Contributing

### Add new project to the list.

To add new FOSS service which accepts crypto, create new issue with "Adding the project {project_name}" or PR to projects.json. I'll add some issue template a bit sooner.

Hosting source somewhere is not enough to be included, though, project must comply with several requirements:
- being developed for more than 1 year (I do not want to review some scam one-day OSS forks which created only with the 
purpose to be included to the list);
- have active and continuously updated git repo;
- have one of the following licences, which fall into the "Free software definition": 
  - Software: MIT, Apache 2.0, BSD, all sorts of GPLs;
  - Websites, content: Creative Commons;
  - SAAS: AGPL.

I'm not a native english speaker, so feel free to adjust content of the site if you find some grammar error. Though is 
should not be a common due to the fact that I try to evaluate the README and descriptions with grammar checking services 
before publishing.

## Author

Dmitry Mantis

## Sponsor

I'm fine, better support some projects from the list! Those awesome people give much more to the world than me. 

