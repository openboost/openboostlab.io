module.exports = function(eleventyConfig) {
    eleventyConfig.addPassthroughCopy("icons/");
    eleventyConfig.addPassthroughCopy("scss/custom.min.css");
    return {
        dir: {
            input: 'templates',
            output: 'public'
        }
    };
};
